﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplicationTest.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Cabecalho = "Este é o texto de Cabeçalho agora enviado através do Controller.";
            ViewBag.ConteudoConhecendo = "Um monte de texto falando sobre mim, etc, etc, etc.";
            ViewBag.BotaoConhecendo = "Saiba mais.";
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "A descrição da minha página, conta com várias informações sobre mim etc Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc non commodo odio, eget fringilla odio. Vivamus laoreet fermentum venenatis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc elit felis, vestibulum sed lobortis vitae, venenatis sed purus. Aenean pellentesque dignissim neque non laoreet. Ut sapien massa, lacinia id ipsum quis, imperdiet iaculis massa. Integer ut porttitor libero, et eleifend massa. Cras sit amet condimentum purus.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Minha página de Contato.";
            ViewBag.Content = "Use this area to provide additional information.";

            return View();
        }
    }
}